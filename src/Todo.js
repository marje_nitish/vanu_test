import React, { Component } from 'react';

import Sidebar from './components/Sidebar';
import Header from './components/Header';
import Form from './components/Form';
import List from './components/List';

const todos = [
    { name: "Make Breakfast" },
    { name: "Office Work" },
    { name: "Shopping" },
    { name: "Coding" }
  ];


class Todo extends Component {

    constructor(props){
        super(props);
        this.state = {
            status: 'Nothing'
        }
    }

    changeStats = () => {
        this.setState({status: 'Updated'});
    }

    render(){
        return (
            <div>
                <Header />
                <p>Status: {this.state.status} </p>
                <Form list={todos} updateStatus={this.changeStats} />
            </div>           
        );
    }

}

export default Todo;