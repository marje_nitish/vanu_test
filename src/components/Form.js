import React, { Component } from "react";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: this.props.list
    };
  }

  handleSubmit = event => {
    event.preventDefault();
    console.log(this.refs.googleInput.value);
    //API call
    let temp = this.state.todos;
    temp.push({ name: this.refs.googleInput.value });
    this.setState(temp);
    this.props.updateStatus();
  };

  componentWillReceiveProps(nextProps) {
    console.log("receiveprops", nextProps);
  }

  componentWillMount() {
    console.log("component will mount");
  }

  componentDidMount() {
    console.log("component did mount");
  }

  componentWillUpdate() {
    console.log("will update", this.state);
  }

  componentDidUpdate() {
    console.log("did update", this.state);
    console.log('API call and pass the state');
  }

  render() {
    return (
      <div className="card" style={{ width: "180rem" }}>
        <div className="card-body">
          <h5 className="card-title">Add Todos</h5>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label>Email address</label>
              <input
                type="text"
                className="form-control"
                placeholder="Todos"
                ref="googleInput"
              />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>

        <div className="card" style={{ width: "180rem", marginTop: "15px" }}>
          <div className="card-body">
            <h5 className="card-title">My Todos</h5>
            <ul className="list-group">
              {this.state.todos.map((row, index) => {
                return (
                  <li className="list-group-item" key={index}>
                    {row.name}
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Form;
