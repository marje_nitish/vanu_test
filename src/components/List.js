import React, { Component } from "react";

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
        todos: [
            { name : 'Make Breakfast'},
            { name : 'Office Work'},
            { name : 'Shopping'},
            { name : 'Coding'},
        ]
    };
  }
  render() {
    return (
      <div class="card" style={{ width: "180rem", marginTop: "15px" }}>
        <div class="card-body">
          <h5 class="card-title">My Todos</h5>
          <ul class="list-group">
            {
                this.state.todos.map((row, index) => {
                    return (
                        <li class="list-group-item">{row.name}</li>
                    );
                })                
            }
          </ul>
        </div>
      </div>
    );
  }
}

export default List;



